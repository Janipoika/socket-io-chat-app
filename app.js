var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var index = require('./routes/index');
var users = require('./routes/users');

var app = express();
app.io = require('socket.io')();


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

app.io.on('connection', function(socket){
  var addedUser = false;
  console.log('a user connected');

  socket.on('new user', function(username){
    if (addedUser) return;
    console.log('new user: ' + username);
    socket.username = username;
    addedUser = true;
  });

  socket.on('chat message', function(msg){
    console.log('new message: ' + msg + ' ' + socket.username);
    socket.broadcast.emit('new message', socket.username, msg);
  });
  socket.on('disconnect', function(){
    console.log('a user connected');
  });
});

module.exports = {app};
